using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public Transform targetFRONT;
    public Transform targetBACK;
    public Transform targetRIGHT;
    public Transform targetLEFT;
    public Transform targetDOWN;
    public Transform targetUP;

    private int layer = 1 << 6;

    private List<GameObject> FrontR = new List<GameObject>();
    private List<GameObject> BackR = new List<GameObject>();
    private List<GameObject> LeftR = new List<GameObject>();
    private List<GameObject> RightR = new List<GameObject>();
    private List<GameObject> UpR = new List<GameObject>();
    private List<GameObject> DownR = new List<GameObject>();

    State state;
    public GameObject go;
    private void Start()
    {   SetrayTransforms(); 
        state = FindObjectOfType<State>();
        ReadState();
        State.started = true;
    }
    private void Update()
    {
       
    }
    public void ReadState()
    {
        state = FindObjectOfType<State>();

        state.up = ReadFace(UpR,targetUP);
        state.down = ReadFace(DownR,targetDOWN);
        state.left = ReadFace(LeftR,targetLEFT);
        state.right = ReadFace(RightR,targetRIGHT);
        state.front = ReadFace(FrontR,targetFRONT);
        state.back= ReadFace(BackR, targetBACK);
    }
    public List<GameObject>ReadFace(List<GameObject> raystarts, Transform rayTransform)
    {
        List<GameObject> sideHit = new List<GameObject>();

        foreach (GameObject raystart in raystarts)
        {
            Vector3 vector = raystart.transform.position;
            RaycastHit hit;
            if (Physics.Raycast(vector, rayTransform.forward, out hit, Mathf.Infinity, layer))
            {
                Debug.DrawRay(vector, rayTransform.forward * hit.distance, Color.yellow);
                sideHit.Add(hit.collider.gameObject);
            }
            else { Debug.DrawRay(vector, rayTransform.forward * 1000, Color.green); }
        }
        

        return sideHit;
    }
    void SetrayTransforms()
    {
        UpR = Rays(targetUP, new Vector3(90, 90, 0));
        DownR = Rays(targetDOWN, new Vector3(270, 90, 0));
        LeftR= Rays(targetLEFT, new Vector3(0,180,0));
        RightR = Rays(targetRIGHT, new Vector3(0, 0, 0));
        FrontR = Rays(targetFRONT, new Vector3(0, 90, 0));
        BackR = Rays(targetBACK, new Vector3(0,270,0));
    }
    List<GameObject> Rays(Transform transformRay, Vector3 way)
    {
        int countRays = 0;
        List<GameObject> rays = new List<GameObject>();
        for (int y = 1; y > -2; y--)
        {
            for (int x = -1; x < 2; x++)
            {
                Vector3 startposition = new Vector3(transformRay.localPosition.x + x, transformRay.localPosition.y + y, transformRay.localPosition.z);
                GameObject startRay = Instantiate(go, startposition, Quaternion.identity, transformRay);
                startRay.name = countRays.ToString();
                rays.Add(startRay);
                countRays++;
            }
        }
        transformRay.localRotation = Quaternion.Euler(way);
        return rays;
    }

}

