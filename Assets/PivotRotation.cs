using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PivotRotation : MonoBehaviour
{
    private List<GameObject> ActualSide;
    private Vector3 ForwardL;
    private Vector3 Mouse;
    private bool moving = false;
    private Quaternion targetQuaternion;
    private float speed = 300f;
    private bool autoRotating = false;
    private float sensitivity = 0.4f;
    private Vector3 rotate;
    private Cube cube;
    private State state;
    void Start()
    {
        cube = FindObjectOfType<Cube>();
        state = FindObjectOfType<State>();
    }

    void LateUpdate()
    {
        if (moving && !autoRotating)
        {
            spinSide(ActualSide);
            if (Input.GetMouseButtonUp(0))
            {
                moving = false;
                Angle();
            }
        }
        if (autoRotating)
        {
            Rotate();
        }
    }
    private void spinSide(List<GameObject> side)
    {
        rotate = Vector3.zero;
        Vector3 mouse = (Input.mousePosition - Mouse);
        if (side == state.up)
        {
            rotate.y = (mouse.x + mouse.y) * sensitivity * -1;
        }
        if (side == state.down)
        {
            rotate.y = (mouse.x + mouse.y) * sensitivity * -1;
        }
        if (side == state.left)
        {
            rotate.z = (mouse.x + mouse.y) * sensitivity * -1;
        }
        if (side == state.right)
        {
            rotate.z = (mouse.x + mouse.y) * sensitivity * -1;
        }
        if (side == state.front)
        {
            rotate.x = (mouse.x + mouse.y) * sensitivity * 1;
        }
        if (side == state.back)
        {
            rotate.x = (mouse.x + mouse.y) * sensitivity * -1;
        }
        transform.Rotate(rotate, Space.Self);
        Mouse = Input.mousePosition;
    }
    public void rotation(List<GameObject> face)
    {
        ActualSide = face;
        Mouse = Input.mousePosition;
        moving = true;
        ForwardL = Vector3.zero - face[4].transform.parent.transform.localPosition;
    }
    public void StartAutoRotate(List<GameObject> side, float angle)
    {
        state.PickUp(side);
        Vector3 localForward = Vector3.zero - side[4].transform.parent.transform.localPosition;
        targetQuaternion = Quaternion.AngleAxis(angle, localForward) * transform.localRotation;
        ActualSide = side;
        autoRotating = true;
    }
    public void Angle()
    {
        Vector3 vec = transform.localEulerAngles;

        vec.x = Mathf.Round(vec.x / 90) * 90;
        vec.y = Mathf.Round(vec.y / 90) * 90;
        vec.z = Mathf.Round(vec.z / 90) * 90;

        targetQuaternion.eulerAngles = vec;
        autoRotating = true;

    }
    private void Rotate()
    {
        moving = false;
        var step = speed * Time.deltaTime;
        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetQuaternion, step);

        if (Quaternion.Angle(transform.localRotation, targetQuaternion) <= 1)
        {
            transform.localRotation = targetQuaternion;
            state.PutDown(ActualSide, transform.parent);
            cube.ReadState();
            State.autoRotating = false;
            autoRotating = false;
            moving = false;
        }
    }
}
