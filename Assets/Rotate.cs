using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
   
    public Vector2 firstPress;
    public Vector2 secondPress;
    public Vector2 currentSwipe;
    public GameObject target;
    public float speed = 100f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Swipe();
        if (transform.rotation!=target.transform.rotation)
        {
            var step = speed * Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, target.transform.rotation, step);
        }
    }
    void Swipe()
    {
        if (Input.GetMouseButtonDown(1))
        {
            firstPress = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(1))
        {
            secondPress = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentSwipe = new Vector2(secondPress.x - firstPress.x, secondPress.y - firstPress.y);
            currentSwipe.Normalize();
            if (LeftSwipe(currentSwipe))
            {
                target.transform.Rotate(0, 90, 0, Space.World);
            }
            else if (RightSwipe(currentSwipe))
            {
                target.transform.Rotate(0, -90, 0, Space.World);
            }
            else if (UpSwipeR(currentSwipe))
            {
                target.transform.Rotate(90, 0, 0, Space.World);
            }
            else if (UpSwipeL(currentSwipe))
            {
                target.transform.Rotate(0, 0, -90, Space.World);
            }
            else if (DownSwipeR(currentSwipe))
            {
                target.transform.Rotate(0, 0, 90, Space.World);
            }
            else if (DownSwipeL(currentSwipe))
            {
                target.transform.Rotate(-90, 0, 0, Space.World);
            }
        }
        
       
    }
    bool LeftSwipe(Vector2 swipe)
    {
        return currentSwipe.x < 0f && currentSwipe.y>-0.5f && currentSwipe.y<0.5f;
    }
    bool RightSwipe(Vector2 swipe)
    {
        return currentSwipe.x > 0f && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f;
    }
    bool UpSwipeR(Vector2 swipe)
    {
        return currentSwipe.y > 0f && currentSwipe.x < 0f;
    }
    bool UpSwipeL(Vector2 swipe)
    {
        return currentSwipe.y > 0f && currentSwipe.x > 0f;
    }
    bool DownSwipeR(Vector2 swipe)
    {
        return currentSwipe.y < 0f && currentSwipe.x < 0f;
    }
    bool DownSwipeL(Vector2 swipe)
    {
        return currentSwipe.y < 0f && currentSwipe.x > 0f;
    }
}
