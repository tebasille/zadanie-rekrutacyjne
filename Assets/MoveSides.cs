using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MoveSides : MonoBehaviour
{
    private State state;
    private Cube cube;
    private int layer = 1 << 6;

    void Start()
    {
        cube=FindObjectOfType<Cube>();
        state=FindObjectOfType<State>();
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            cube.ReadState();
            RaycastHit raycastHit;
            Ray ray=Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray,out raycastHit, 100.0f, layer))
            {
                GameObject face=raycastHit.collider.gameObject;
                List<List<GameObject>> cubesides = new List<List<GameObject>>()
                {
                    state.up,
                    state.down,
                    state.left,
                    state.right,
                    state.front,
                    state.back
                };
                foreach (List<GameObject> cubeSide in cubesides)
                {
                    if (cubeSide.Contains(face))
                    {
                        state.PickUp(cubeSide);
                        foreach (var side in cubeSide)
                        {
                            print(side.name);
                        }
                        Debug.Log(cubeSide);
                        cubeSide[4].transform.parent.GetComponent<PivotRotation>().rotation(cubeSide);
                    }
                }
            }
        }
    }
    
}
